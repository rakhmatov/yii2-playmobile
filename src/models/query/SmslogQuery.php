<?php

namespace rakhmatov\playmobile\models\query;

/**
 * This is the ActiveQuery class for [[rakhmatov\playmobile\models\Smslog]].
 *
 * @see rakhmatov\playmobile\models\Smslog
 */
class SmslogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \rakhmatov\playmobile\models\Smslog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \rakhmatov\playmobile\models\Smslog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
